import 'package:flutter/material.dart';

class AppColors {
  static const Color app_Simply = Color(0xFF1B2A3B);
  static const Color app_Select = Color(0xFFF1F4FF);
  static const Color app_button_color = Color(0xFFFF8080);
  static const Color app_button_color_disabled = Color(0xFF586BCA);
  static const Color arrow_back = Color(0xFF707070);
  static const Color text_field_labelText_color = Color(0xFF333A42);
  static const Color text_forgot_password_color = Color(0xFF333A42);
  static const Color text_Border_color = Color(0xFFD8D8D8);
  static const Color shadow_color = Color(0xFFE9E7F1);
  static const Color app_bottom_navigation = Color(0xFFFF6B6B);
  static const kSecondaryColor = Color(0xFF979797);
}
