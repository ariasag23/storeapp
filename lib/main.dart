import 'package:flutter/material.dart';
import 'package:store_app/Screens/auth/SignIn_Screen.dart';
import 'package:store_app/Screens/auth/SignUp_Screen.dart';
import 'package:store_app/bn_Screens/bottom_navigation_main_screen.dart';
import 'package:store_app/bn_Screens/profile/payments/payment.dart';
import 'package:store_app/details/details_screen.dart';

import 'out_boarding/out_boarding_screen.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/out_boarding_screen',
        routes: {
          // out_boarding_screen
          '/out_boarding_screen': (context) => OutBoardingScreen(),

          // auth
          '/SignIn_Screen': (context) => SignIn(),
          '/SignUp_Screen': (context) => SignUp(),

          //BottomNavigationMainScreen
          '/BottomNavigationMainScreen': (context) =>
              BottomNavigationMainScreen(),

          //Details
          '/DetailsScreen': (context) => DetailsScreen(),

          // Profile
          '/Payment': (context) => Payment(),
        });
  }
}
