import 'package:flutter/material.dart';

import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/utils/size_config.dart';

class AppElevatedButton extends StatelessWidget {
  final String title;
  final void Function() onPressed;
  final double size;
  final Color colorText;
  final double width;
  final Color color;
  final double borderRadius;

  AppElevatedButton(
      {required this.title,
      required this.onPressed,
      this.size = 50,
      this.width = 135,
      this.colorText = AppColors.app_button_color,
      this.color = AppColors.app_button_color,
      this.borderRadius = 0});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Text(
        title,
        style: TextStyle(color: colorText),
      ),
      style: ElevatedButton.styleFrom(
        side: BorderSide(
            width: SizeConfig.scaleWidth(1), color: AppColors.app_button_color),
        primary: color,
        minimumSize: Size(
          width,
          SizeConfig.scaleHeight(size),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(borderRadius),
        ),
      ),
    );
  }
}
