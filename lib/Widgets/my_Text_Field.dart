import 'package:flutter/material.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/utils/size_config.dart';

class MyTextField extends StatelessWidget {
  final TextEditingController? controller;
  final bool obscureText;
  final TextInputType textInputType;
  final String labelText;

  MyTextField(
      {this.controller,
      this.obscureText = false,
      this.textInputType = TextInputType.text,
      required this.labelText});

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      obscureText: obscureText,
      keyboardType: textInputType,
      style: TextStyle(
          color: AppColors.arrow_back,
          fontSize: SizeConfig.scaleTextFont(24),
          fontFamily: 'Roboto'),
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: TextStyle(
            color: AppColors.text_field_labelText_color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w100),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: AppColors.arrow_back, width: 1),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: AppColors.arrow_back, width: 1),
        ),
      ),
    );
  }
}
