import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';

import 'package:store_app/utils/size_config.dart';

class CategoryCard extends StatelessWidget {
  late String icon;
  late Color color;
  // late String? text;
  late GestureTapCallback? press;

  CategoryCard(
      {required this.icon,
      required this.color,
      // required this.text,
      this.press});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: SizedBox(
        width: SizeConfig.scaleWidth(55),
        child: Column(
          children: [
            Container(
              padding: EdgeInsetsDirectional.only(
                top: SizeConfig.scaleHeight(15),
                start: SizeConfig.scaleWidth(15),
                end: SizeConfig.scaleWidth(15),
                bottom: SizeConfig.scaleHeight(15),
              ),
              height: SizeConfig.scaleHeight(55),
              width: SizeConfig.scaleWidth(55),
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Image.asset(icon),
            ),
            SizedBox(height: 5),
            // Text(text, textAlign: TextAlign.center)
          ],
        ),
      ),
    );
  }
}
