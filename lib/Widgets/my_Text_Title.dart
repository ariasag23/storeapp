import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

class MyTextTitle extends StatelessWidget {
  final String text;
  final Color color;
  final FontWeight fontWeight;
  final double size;
  final String? fontFamily;
  final TextAlign textAlign;
  final int? maxLines;

  MyTextTitle(
      {required this.text,
      this.color = Colors.black,
      this.fontWeight = FontWeight.normal,
      this.size = 16,
      this.fontFamily,
      this.textAlign = TextAlign.left,
      this.maxLines});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxLines,
      style: TextStyle(
          color: color,
          fontWeight: fontWeight,
          fontSize: SizeConfig.scaleTextFont(size)),
      textAlign: textAlign,
    );
  }
}
