import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

class SpecialOfferCard extends StatelessWidget {
  late String category;
  late String image;
  late int numOfBrands;
  late GestureTapCallback press;
  SpecialOfferCard(
      {required this.category,
      required this.image,
      required this.numOfBrands,
      required this.press});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(end: SizeConfig.scaleWidth(20)),
      child: GestureDetector(
        onTap: press,
        child: SizedBox(
          width: SizeConfig.scaleWidth(242),
          height: SizeConfig.scaleHeight(100),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Stack(
              children: [
                Image.asset(
                  image,
                  fit: BoxFit.cover,
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xFF343434).withOpacity(0.4),
                        Color(0xFF343434).withOpacity(0.15),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 15.0,
                    vertical: 10,
                  ),
                  child: Text.rich(
                    TextSpan(
                      style: TextStyle(color: Colors.white),
                      children: [
                        TextSpan(
                          text: "$category\n",
                          style: TextStyle(
                            fontSize: SizeConfig.scaleTextFont(18),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(text: "$numOfBrands Brands")
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
