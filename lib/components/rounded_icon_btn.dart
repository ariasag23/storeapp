import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

class RoundedIconBtn extends StatelessWidget {
  final IconData icon;
  final GestureTapCancelCallback press;
  final bool showShadow;
  RoundedIconBtn(
      {required this.icon, required this.press, this.showShadow = false});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: press,
      child: Container(
        height: SizeConfig.scaleHeight(40),
        width: SizeConfig.scaleWidth(40),
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          boxShadow: [
            if (showShadow)
              BoxShadow(
                offset: Offset(0, 5),
                blurRadius: 10,
                color: Color(0xFFB0B0B0).withOpacity(0.2),
              ),
          ],
        ),
        child: Icon(
          icon,
          color: Colors.black,
        ),
      ),
    );
  }
}
