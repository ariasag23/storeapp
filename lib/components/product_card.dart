import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/models/Product.dart';
import 'package:store_app/utils/size_config.dart';

class ProductCard extends StatelessWidget {
  const ProductCard(
      {Key? key,
      this.width = 140,
      this.aspectRetio = 1.02,
      required this.product})
      : super(key: key);

  final double width, aspectRetio;
  final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
      child: SizedBox(
        width: SizeConfig.scaleWidth(width),
        child: GestureDetector(
          onTap: () => {Navigator.pushNamed(context, '/DetailsScreen')},
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1.02,
                child: Container(
                  padding: EdgeInsetsDirectional.only(
                    top: SizeConfig.scaleHeight(20),
                    start: SizeConfig.scaleWidth(20),
                    end: SizeConfig.scaleWidth(20),
                    bottom: SizeConfig.scaleHeight(20),
                  ),
                  decoration: BoxDecoration(
                    color: AppColors.kSecondaryColor.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Hero(
                    tag: product.id.toString(),
                    child: Image.asset(product.images[0]),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Text(
                product.title,
                style: TextStyle(color: Colors.black),
                maxLines: 2,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "\$${product.price}",
                    style: TextStyle(
                      fontSize: SizeConfig.scaleTextFont(18),
                      fontWeight: FontWeight.w600,
                      color: AppColors.app_button_color,
                    ),
                  ),
                  InkWell(
                    borderRadius: BorderRadius.circular(50),
                    onTap: () {},
                    child: Container(
                      padding: EdgeInsetsDirectional.only(
                        top: SizeConfig.scaleHeight(8),
                        start: SizeConfig.scaleWidth(8),
                        end: SizeConfig.scaleWidth(8),
                        bottom: SizeConfig.scaleHeight(8),
                      ),
                      height: SizeConfig.scaleHeight(28),
                      width: SizeConfig.scaleWidth(28),
                      decoration: BoxDecoration(
                        color: product.isFavourite
                            ? AppColors.app_button_color.withOpacity(0.15)
                            : AppColors.kSecondaryColor.withOpacity(0.1),
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset(
                        "assets/icons/Heart Icon_2.svg",
                        color: product.isFavourite
                            ? Color(0xFFFF4848)
                            : Color(0xFFDBDEE4),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
