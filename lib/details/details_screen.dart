import 'package:flutter/material.dart';
import 'package:store_app/Widgets/app_elevated_button.dart';
import 'package:store_app/details/components/ProductDescription.dart';
import 'package:store_app/details/components/body.dart';
import 'package:store_app/details/components/color_dots.dart';
import 'package:store_app/details/components/custom_app_bar.dart';
import 'package:store_app/details/components/top_rounded_container.dart';
import 'package:store_app/utils/size_config.dart';

class DetailsScreen extends StatefulWidget {
  const DetailsScreen({Key? key}) : super(key: key);

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6F7F9),
      body: ListView(children: [
        CustomAppBar(
          rating: 4.8,
        ),
        Body(),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              ProductDescription(),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    ColorDots(),
                    TopRoundedContainer(
                      color: Color(0xFFF6F7F9),
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: SizeConfig.scaleWidth(30),
                          right: SizeConfig.scaleWidth(30),
                        ),
                        child: AppElevatedButton(
                            title: 'Add To Cart',
                            colorText: Colors.white,
                            borderRadius: 20,
                            onPressed: () {}),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
