import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/utils/size_config.dart';

class ProductDescription extends StatelessWidget {
  const ProductDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(20)),
      padding: EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(20)),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40), topRight: Radius.circular(40)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: (20)),
            child: Text(
              'Nike Sport White - Man Pant',
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              padding: EdgeInsets.all((15)),
              width: (64),
              decoration: BoxDecoration(
                color: Color(0xFFFFE6E6),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                ),
              ),
              child: SvgPicture.asset(
                "assets/icons/Heart Icon_2.svg",
                color: Color(0xFFFF4848),
                height: (16),
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(50),
          ),
          Padding(
            padding: EdgeInsets.only(left: (20), right: (64), bottom: (40)),
            child: MyTextTitle(
              text:
                  'Amazon Essentials is focused on creating affordable, high-quality, and long-lasting everyday clothing you can rely on. Our line of men’s must-haves includes polo shirts, chino pants, classic-fit shorts, casual button-downs, and crew-neck tees. Our consistent sizing takes the guesswork out of shopping, and each piece is put to the test to maintain the highest standards in quality and comfort.',
              maxLines: 3,
            ),
          ),
        ],
      ),
    );
  }
}
