import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

class TopRoundedContainer extends StatelessWidget {
  final Color color;
  final Widget child;

  TopRoundedContainer({required this.color, required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(20)),
      padding: EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(20)),
      width: double.infinity,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40),
          topRight: Radius.circular(40),
        ),
      ),
      child: child,
    );
  }
}
