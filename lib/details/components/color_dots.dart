import 'package:flutter/material.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/components/rounded_icon_btn.dart';

import 'package:store_app/utils/size_config.dart';

class ColorDots extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: (20)),
      child: Row(
        children: [
          MyTextTitle(
            text: 'Product',
            fontWeight: FontWeight.bold,
            size: 20,
          ),
          Spacer(),
          RoundedIconBtn(
            icon: Icons.remove,
            showShadow: true,
            press: () {},
          ),
          SizedBox(width: SizeConfig.scaleWidth(10)),
          Container(
            height: SizeConfig.scaleHeight(40),
            width: SizeConfig.scaleWidth(40),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, 2),
                  blurRadius: 10,
                  color: Color(0xFFB0B0B0).withOpacity(0.2),
                ),
              ],
            ),
            child: Center(
              child: MyTextTitle(
                text: '1',
                fontWeight: FontWeight.bold,
                size: 18,
              ),
            ),
          ),
          SizedBox(width: SizeConfig.scaleWidth(10)),
          RoundedIconBtn(
            icon: Icons.add,
            showShadow: true,
            press: () {},
          ),
        ],
      ),
    );
  }
}
