import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: SizeConfig.scaleWidth(238),
          child: AspectRatio(
            aspectRatio: 1,
            child: Image.asset("assets/images/Image Popular Product 2.png"),
          ),
        ),
      ],
    );
  }
}
