import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/utils/size_config.dart';

import '../../utils/size_config.dart';

class CustomAppBar extends StatelessWidget {
  final double rating;
  CustomAppBar({required this.rating});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: [
          SizedBox(
              height: SizeConfig.scaleHeight(40),
              width: SizeConfig.scaleWidth(40),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                ),
              )),
          Spacer(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 14, vertical: 5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
            ),
            child: Row(
              children: [
                MyTextTitle(
                  text: '4.1',
                  size: 14,
                  fontWeight: FontWeight.w600,
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(5),
                ),
                SvgPicture.asset('assets/icons/Star Icon.svg'),
              ],
            ),
          )
        ],
      ),
    );
  }
}
