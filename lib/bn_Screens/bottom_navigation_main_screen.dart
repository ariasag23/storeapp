import 'package:flutter/material.dart';
import 'package:store_app/bn_Screens/cart/cart_screen.dart';
import 'package:store_app/bn_Screens/favorite/favorite_Screen.dart';
import 'package:store_app/bn_Screens/home.dart';
import 'package:store_app/bn_Screens/home/components/body.dart';
import 'package:store_app/bn_Screens/profile/profile_screen.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/models/bn_screen.dart';

class BottomNavigationMainScreen extends StatefulWidget {
  const BottomNavigationMainScreen({Key? key}) : super(key: key);

  @override
  _BottomNavigationMainScreenState createState() =>
      _BottomNavigationMainScreenState();
}

class _BottomNavigationMainScreenState
    extends State<BottomNavigationMainScreen> {
  int _currentIndex = 0;

  List<BnScreen> _bnScreens = <BnScreen>[
    const BnScreen('Home', Body()),
    const BnScreen('Cart', CartScreen()),
    const BnScreen('Favorite', Favorite()),
    const BnScreen('User', ProfileScreen()),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (int selectedItemIndex) {
          setState(() {
            _currentIndex = selectedItemIndex;
          });
        },
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        selectedIconTheme:
            IconThemeData(color: AppColors.app_bottom_navigation),
        selectedItemColor: AppColors.app_bottom_navigation,
        unselectedIconTheme: IconThemeData(color: Color(0xFFB9BDC8)),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.card_travel_outlined),
            label: 'Cart',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite_border_outlined),
            label: 'Favorite',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle_outlined),
            label: 'User',
          ),
        ],
      ),
      body: _getScreen(),
    );
  }

  Widget _getScreen() => _bnScreens.elementAt(_currentIndex).screen;
}
