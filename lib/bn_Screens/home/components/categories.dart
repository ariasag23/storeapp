import 'package:flutter/material.dart';

import 'package:store_app/Widgets/my_CategoryCard.dart';
import 'package:store_app/bn_Screens/home/components/section_title.dart';
import 'package:store_app/utils/size_config.dart';

class Categories extends StatelessWidget {
  const Categories({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> categories = [
      {"icon": "assets/categories/shirt.png", "color": Color(0xFFF38cfdd)},
      {"icon": "assets/categories/dress.png", "color": Color(0xfff38cdd)},
      {"icon": "assets/categories/shoes.png", "color": Color(0xff4ff2af)},
      {"icon": "assets/categories/pant.png", "color": Color(0xfffc6c8d)},
      {"icon": "assets/categories/technology.png", "color": Color(0xff74acf7)},
    ];
    return Column(children: [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: SectionTitle(
          title: "categories",
          hint: "All",
          press: () {},
        ),
      ),
      SizedBox(height: SizeConfig.scaleHeight(20)),
      Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
          bottom: SizeConfig.scaleHeight(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: List.generate(
            categories.length,
            (index) => CategoryCard(
              color: categories[index]["color"],
              icon: categories[index]["icon"],
              //  text: categories[index]["text"],
              press: () {},
            ),
          ),
        ),
      ),
    ]);
  }
}
