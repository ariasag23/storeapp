import 'package:flutter/material.dart';
import 'package:store_app/bn_Screens/home/components/section_title.dart';
import 'package:store_app/components/product_card.dart';
import 'package:store_app/models/Product.dart';
import 'package:store_app/utils/size_config.dart';

class PopularProducts extends StatefulWidget {
  const PopularProducts({Key? key}) : super(key: key);

  @override
  _PopularProductsState createState() => _PopularProductsState();
}

class _PopularProductsState extends State<PopularProducts> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SectionTitle(title: "Popular Products", press: () {}),
        ),
        SizedBox(height: SizeConfig.scaleHeight(20)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ...List.generate(
                demoProducts.length,
                (index) {
                  if (demoProducts[index].isPopular)
                    return ProductCard(product: demoProducts[index]);

                  return SizedBox
                      .shrink(); // here by default width and height is 0
                },
              ),
              SizedBox(width: SizeConfig.scaleWidth(20)),
            ],
          ),
        )
      ],
    );
  }
}
