import 'package:flutter/material.dart';
import 'package:store_app/bn_Screens/home/components/banners.dart';
import 'package:store_app/bn_Screens/home/components/categories.dart';
import 'package:store_app/bn_Screens/home/components/home_header.dart';
import 'package:store_app/bn_Screens/home/components/popular_product.dart';
import 'package:store_app/bn_Screens/home/components/special_offers.dart';
import 'package:store_app/utils/size_config.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(children: [
          SizedBox(height: SizeConfig.scaleHeight(20)),
          HomeHeader(),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Banners(),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Categories(),
          SpecialOffers(),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          PopularProducts(),
        ]),
      ),
    );
  }
}
