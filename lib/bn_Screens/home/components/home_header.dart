import 'package:flutter/material.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/bn_Screens/home/components/search_field.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/utils/size_config.dart';

class HomeHeader extends StatefulWidget {
  const HomeHeader({Key? key}) : super(key: key);

  @override
  _HomeHeaderState createState() => _HomeHeaderState();
}

class _HomeHeaderState extends State<HomeHeader> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleHeight(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SearchField(),
          CircleAvatar(
            backgroundColor: AppColors.kSecondaryColor.withOpacity(0.1),
            radius: 32,
            child: //Image.asset('assets/images/ecommerce.png',fit: BoxFit.fill,),

                MyTextTitle(text: 'M', size: 22, color: Colors.black),
          )
        ],
      ),
    );
  }
}
