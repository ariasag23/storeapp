import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

class SectionTitle extends StatelessWidget {
  late String title;
  late GestureTapCallback press;
  late String hint;

  SectionTitle({
    required this.title,
    this.hint = "See More",
    required this.press,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: SizeConfig.scaleTextFont(18),
            color: Colors.black,
          ),
        ),
        GestureDetector(
          onTap: press,
          child: Text(
            hint,
            style: TextStyle(color: Color(0xFFBBBBBB)),
          ),
        ),
      ],
    );
  }
}
