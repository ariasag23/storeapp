import 'package:flutter/material.dart';
import 'package:store_app/Widgets/my_SpecialOfferCard.dart';
import 'package:store_app/bn_Screens/home/components/section_title.dart';
import 'package:store_app/utils/size_config.dart';

class SpecialOffers extends StatefulWidget {
  const SpecialOffers({Key? key}) : super(key: key);

  @override
  _SpecialOffersState createState() => _SpecialOffersState();
}

class _SpecialOffersState extends State<SpecialOffers> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SectionTitle(
            title: "Special for you",
            press: () {},
          ),
        ),
        SizedBox(height: SizeConfig.scaleHeight(20)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: EdgeInsetsDirectional.only(
              start: SizeConfig.scaleWidth(15),
            ),
            child: Row(
              children: [
                SpecialOfferCard(
                  image: "assets/images/Image Banner 2.png",
                  category: "Smartphone",
                  numOfBrands: 18,
                  press: () {},
                ),
                SpecialOfferCard(
                  image: "assets/images/Image Banner 3.png",
                  category: "Fashion",
                  numOfBrands: 24,
                  press: () {},
                ),
                SizedBox(width: SizeConfig.scaleWidth(20)),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
