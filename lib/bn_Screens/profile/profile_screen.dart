import 'package:flutter/material.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/bn_Screens/profile/components/body.dart';
import 'package:store_app/utils/size_config.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        title: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(90)),
          child: MyTextTitle(
            text: 'Profile',
            fontWeight: FontWeight.bold,
            size: 24,
          ),
        ),
      ),
      body: Body(),
    );
  }
}
