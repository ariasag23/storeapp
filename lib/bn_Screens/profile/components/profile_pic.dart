import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:store_app/utils/size_config.dart';

class ProfilePic extends StatelessWidget {
  const ProfilePic({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: SizeConfig.scaleWidth(115),
          height: SizeConfig.scaleHeight(115),
          child: Stack(
            fit: StackFit.expand,
            clipBehavior: Clip.hardEdge,
            children: [
              CircleAvatar(
                radius: 45,
                backgroundColor: Color(0xFFF5F6F9),
                backgroundImage: AssetImage('assets/images/profile.jpeg'),
              ),
              Positioned(
                right: 0,
                bottom: 10,
                child: SizedBox(
                  width: SizeConfig.scaleWidth(40),
                  height: SizeConfig.scaleHeight(40),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        side: BorderSide(color: Colors.white),
                        primary: Color(0xFFF5F6F9),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                      ),
                      onPressed: () {},
                      child: SvgPicture.asset(
                        'assets/icons/Camera Icon.svg',
                        width: 22,
                      )),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
