import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/utils/size_config.dart';

class ProfileMenu extends StatelessWidget {
  final String text;
  final String icon;
  final VoidCallback? press;

  ProfileMenu({required this.text, required this.icon, this.press});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        child: Card(
          elevation: 1,
          color: Color(0xFFF5F6F9),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Padding(
            padding: EdgeInsetsDirectional.only(
              top: SizeConfig.scaleHeight(20),
              start: SizeConfig.scaleWidth(20),
              end: SizeConfig.scaleWidth(20),
              bottom: SizeConfig.scaleHeight(20),
            ),
            child: Row(
              children: [
                SvgPicture.asset(
                  icon,
                  color: AppColors.app_Simply,
                  width: 22,
                ),
                SizedBox(width: 20),
                Expanded(
                    child: MyTextTitle(
                  text: text,
                  color: AppColors.app_Simply,
                )),
                InkWell(
                  onTap: press,
                  child: Icon(
                    Icons.arrow_forward_ios,
                    color: AppColors.app_Simply,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
