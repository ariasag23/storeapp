import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:store_app/Widgets/app_elevated_button.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/bn_Screens/profile/components/profile_menu.dart';
import 'package:store_app/bn_Screens/profile/components/profile_pic.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          ProfilePic(),
          SizedBox(height: 10),
          MyTextTitle(
            text: 'Ahmed Ghayada',
            color: AppColors.app_Simply,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 30),
          Divider(
            color: AppColors.app_Simply,
            height: 3,
            thickness: 1,
            indent: 30,
            endIndent: 30,
          ),
          SizedBox(height: 20),
          ProfileMenu(
            text: "My Account",
            icon: "assets/icons/User Icon.svg",
            press: () => {},
          ),
          ProfileMenu(
            text: "Payments",
            icon: "assets/icons/Cash.svg",
            press: () {
              Navigator.pushNamed(context, '/Payment');
            },
          ),
          ProfileMenu(
            text: "Settings",
            icon: "assets/icons/Settings.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Help Center",
            icon: "assets/icons/Question mark.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Log Out",
            icon: "assets/icons/Log out.svg",
            press: () {},
          ),
        ],
      ),
    );
  }
}
