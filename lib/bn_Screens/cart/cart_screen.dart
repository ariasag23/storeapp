import 'package:flutter/material.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/bn_Screens/cart/components/body.dart';
import 'package:store_app/bn_Screens/cart/components/check_out_card.dart';
import 'package:store_app/models/Cart.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {}),
        title: Column(
          children: [
            MyTextTitle(
              text: 'Your Cart',
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
            MyTextTitle(
              text: '${demoCarts.length} items',
              fontWeight: FontWeight.w600,
              color: Colors.grey,
            ),
          ],
        ),
      ),
      body: Body(),
      bottomNavigationBar: CheckoutCard(),
    );
  }
}
