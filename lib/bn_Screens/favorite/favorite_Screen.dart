import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/components/product_card.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/utils/size_config.dart';

class Favorite extends StatefulWidget {
  const Favorite({Key? key}) : super(key: key);

  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {}),
        title: MyTextTitle(
          text: 'Favorite',
          color: AppColors.app_Simply,
        ),
      ),
      body: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 20,
              mainAxisSpacing: 10,
              childAspectRatio: 0.85),
          itemCount: 8,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext ctx, index) {
            return Padding(
              padding: EdgeInsetsDirectional.only(
                  start: SizeConfig.scaleWidth(20),
                  end: SizeConfig.scaleWidth(20)),
              child: SizedBox(
                width: SizeConfig.scaleWidth(140),
                child: GestureDetector(
                  onTap: () => {Navigator.pushNamed(context, '/DetailsScreen')},
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AspectRatio(
                        aspectRatio: 1.02,
                        child: Container(
                          padding: EdgeInsetsDirectional.only(
                            top: SizeConfig.scaleHeight(20),
                            start: SizeConfig.scaleWidth(20),
                            end: SizeConfig.scaleWidth(20),
                            bottom: SizeConfig.scaleHeight(20),
                          ),
                          decoration: BoxDecoration(
                            color: AppColors.kSecondaryColor.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Hero(
                              tag: 1,
                              child: Image.asset(
                                  "assets/images/Image Popular Product 2.png")),
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Nike Sport White - Man Pant',
                        style: TextStyle(color: Colors.black),
                        maxLines: 2,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "\$${50.5}",
                            style: TextStyle(
                              fontSize: SizeConfig.scaleTextFont(18),
                              fontWeight: FontWeight.w600,
                              color: AppColors.app_button_color,
                            ),
                          ),
                          InkWell(
                            borderRadius: BorderRadius.circular(50),
                            onTap: () {},
                            child: Container(
                              padding: EdgeInsetsDirectional.only(
                                top: SizeConfig.scaleHeight(8),
                                start: SizeConfig.scaleWidth(8),
                                end: SizeConfig.scaleWidth(8),
                                bottom: SizeConfig.scaleHeight(8),
                              ),
                              height: SizeConfig.scaleHeight(28),
                              width: SizeConfig.scaleWidth(28),
                              decoration: BoxDecoration(
                                color: AppColors.app_button_color
                                    .withOpacity(0.15),
                                shape: BoxShape.circle,
                              ),
                              child: SvgPicture.asset(
                                  "assets/icons/Heart Icon_2.svg",
                                  color: Color(0xFFFF4848)),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
