import 'package:flutter/material.dart';
import 'package:store_app/Widgets/app_elevated_button.dart';
import 'package:store_app/Widgets/my_Text_Field.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/utils/size_config.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: AppColors.arrow_back,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: MyTextTitle(
          text: 'SignUp',
          color: AppColors.app_Simply,
          fontWeight: FontWeight.bold,
          size: 24,
        ),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
            top: SizeConfig.scaleHeight(60),
            start: SizeConfig.scaleWidth(27),
            end: SizeConfig.scaleWidth(27)),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            MyTextField(
              labelText: 'Name',
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
            MyTextField(
              labelText: 'Email',
              textInputType: TextInputType.emailAddress,
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
            MyTextField(
              labelText: 'Password',
              obscureText: true,
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
            MyTextField(
              labelText: 'Phone number',
              textInputType: TextInputType.number,
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(33),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(56),
            ),
            AppElevatedButton(
                title: 'SIGN UP',
                colorText: Colors.white,
                width: double.infinity,
                onPressed: () {}),
            SizedBox(
              height: SizeConfig.scaleHeight(41),
            ),
            MyTextTitle(
              text:
                  'By clicking Sign Up you are in agreement of the \n Terms and Conditions',
              size: 12,
              fontWeight: FontWeight.w600,
              color: AppColors.app_Simply,
              textAlign: TextAlign.center,
            ),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyTextTitle(
                  text: 'Already have acount?',
                  color: AppColors.app_Simply,
                  size: 16,
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(10),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/SignIn_Screen');
                  },
                  child: Container(
                    width: SizeConfig.scaleWidth(100),
                    height: SizeConfig.scaleHeight(30),
                    margin: EdgeInsetsDirectional.zero,
                    padding: EdgeInsetsDirectional.zero,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: AppColors.text_Border_color,
                          width: SizeConfig.scaleWidth(1),
                        )),
                    child: Center(
                      child: MyTextTitle(
                        text: 'LOGIN',
                        color: AppColors.app_Simply,
                        size: 12,
                        fontWeight: FontWeight.bold,
                        // textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  style: TextButton.styleFrom(padding: EdgeInsets.zero),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
