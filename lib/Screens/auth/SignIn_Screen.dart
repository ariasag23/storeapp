import 'package:flutter/material.dart';
import 'package:store_app/Widgets/app_elevated_button.dart';
import 'package:store_app/Widgets/my_Text_Field.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/utils/size_config.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: AppColors.arrow_back,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: MyTextTitle(
          text: 'Login',
          color: AppColors.app_Simply,
          fontWeight: FontWeight.bold,
          size: 24,
        ),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
            top: SizeConfig.scaleHeight(60),
            start: SizeConfig.scaleWidth(27),
            end: SizeConfig.scaleWidth(27)),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            MyTextField(
              labelText: 'Email',
              textInputType: TextInputType.emailAddress,
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
            MyTextField(
              labelText: 'Password',
              obscureText: true,
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(33),
            ),
            TextButton(
                onPressed: () {},
                child: MyTextTitle(
                  text: 'Forgot your password?',
                  color: AppColors.text_forgot_password_color,
                  size: 12,
                  textAlign: TextAlign.center,
                )),
            SizedBox(
              height: SizeConfig.scaleHeight(67),
            ),
            AppElevatedButton(
                title: 'LOGIN',
                colorText: Colors.white,
                width: double.infinity,
                onPressed: () {
                  Navigator.pushNamed(context, '/BottomNavigationMainScreen');
                }),
            SizedBox(
              height: SizeConfig.scaleHeight(72),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MyTextTitle(
                  text: 'Or login with',
                  color: AppColors.app_Simply,
                  size: 12,
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(20),
                ),
                Image.asset(
                  'assets/images/facebook.png',
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(20),
                ),
                Image.asset(
                  'assets/images/google.png',
                  fit: BoxFit.cover,
                ),
              ],
            ),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyTextTitle(
                  text: 'Don\'t have acount?',
                  color: AppColors.app_Simply,
                  size: 16,
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(10),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/SignUp_Screen');
                  },
                  child: Container(
                    width: SizeConfig.scaleWidth(100),
                    height: SizeConfig.scaleHeight(30),
                    margin: EdgeInsetsDirectional.zero,
                    padding: EdgeInsetsDirectional.zero,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: AppColors.text_Border_color,
                          width: SizeConfig.scaleWidth(1),
                        )),
                    child: Center(
                      child: MyTextTitle(
                        text: 'SIGN UP',
                        color: AppColors.app_Simply,
                        size: 12,
                        fontWeight: FontWeight.bold,
                        // textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  style: TextButton.styleFrom(padding: EdgeInsets.zero),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
