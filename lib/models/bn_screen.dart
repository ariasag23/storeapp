import 'package:flutter/material.dart';

class BnScreen {
  final String _title;
  final Widget _screen;

  const BnScreen(this._title, this._screen);

  Widget get screen => _screen;

  String get title => _title;
}
