import 'package:flutter/material.dart';
import 'package:store_app/Widgets/app_elevated_button.dart';
import 'package:store_app/Widgets/my_Text_Title.dart';
import 'package:store_app/extenssions/app_colors_extenssion.dart';
import 'package:store_app/utils/size_config.dart';

class OutBoardingScreen extends StatefulWidget {
  const OutBoardingScreen({Key? key}) : super(key: key);

  @override
  _OutBoardingScreenState createState() => _OutBoardingScreenState();
}

class _OutBoardingScreenState extends State<OutBoardingScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MyTextTitle(
            text: 'Simply',
            color: AppColors.app_Simply,
            size: 25,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(12),
          ),
          MyTextTitle(
            text: 'Select your photographer,\n then go to session!',
            fontWeight: FontWeight.w600,
            size: 16,
            color: AppColors.app_Simply,
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(40),
          ),
          Image.asset(
            'assets/images/ecommerce.png',
            fit: BoxFit.cover,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(40),
          ),
          Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppElevatedButton(
                    title: 'Sign in',
                    colorText: Colors.white,
                    borderRadius: 10,
                    onPressed: () {
                      Navigator.pushNamed(context, '/SignIn_Screen');
                    }),
                SizedBox(
                  width: SizeConfig.scaleWidth(15),
                ),
                AppElevatedButton(
                    title: 'Sign up',
                    colorText: AppColors.app_Simply,
                    color: Colors.white,
                    borderRadius: 10,
                    onPressed: () {
                      Navigator.pushNamed(context, '/SignUp_Screen');
                    }),
              ])
        ],
      ),
    );
  }
}
